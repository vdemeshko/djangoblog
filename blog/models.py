from django.db import models

# Create your models here.


class Post(models.Model):
    title = models.CharField(max_length=255)    # post title
    datetime = models.DateTimeField(u'Post date')     # post date
    content = models.TextField(max_length=10000)    # post context

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/blog/%i/" % self.id