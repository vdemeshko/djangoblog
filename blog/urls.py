# coding: utf-8
from django.conf.urls import patterns, url

from blog.views import PostsListView, PostDetailView

urlpatterns = patterns('',
                       # URL http://site_name/blog/
                       url(r'^$', PostsListView.as_view(), name='list'),
                       # URL http://site_name/blog/post_number/
                       url(r'^(?P<pk>\d+)/$', PostDetailView.as_view()),

                       )
