from django.shortcuts import render
from blog.models import Post
from django.views.generic import ListView, DetailView
# Create your views here.


class PostsListView(ListView):  # representation as view
    model = Post                   # model for representation


class PostDetailView(DetailView):   # detailed representation of the model
    model = Post